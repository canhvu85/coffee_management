<%--
  Created by IntelliJ IDEA.
  User: BVMT
  Date: 12/11/2019
  Time: 10:55 SA
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Deleting Employees</title>
</head>
<body>
<h1>Delete Employees</h1>
<p>
    <a href="${pageContext.request.contextPath}/employees">Back to Employees list</a>
</p>
<form method="post">
    <h3>Are you sure?</h3>
    <fieldset>
        <legend>Employees information</legend>
        <table>
        <tr>
            <tr>
                <td>Name: </td>
                <td>${requestScope["employee"].getName()}</td>
            </tr>
            <tr>
                <td>Password: </td>
                <td>${requestScope["employee"].getPassword()}</td>
            </tr>
            <tr>
                <td>Birthday: </td>
                <td>${requestScope["employee"].getBirthday()}</td>
            </tr>
            <tr>
                <td>IdCard: </td>
                <td>${requestScope["employee"].getIdEmployee()}</td>
            </tr>
            <tr>
                <td>Authority:</td>
                <td>
                    <c:choose>
                    <c:when test="${employee.getAuthority()}">
                        Chu
                    </c:when>
                    <c:otherwise>
                        Nhan Vien
                    </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <tr>
                <td>Gender</td>
                <td><c:choose>
                    <c:when test="${employee.getGender()}">
                        Nam
                    </c:when>
                    <c:otherwise>
                        Nu
                    </c:otherwise>
                </c:choose></td>
            </tr>
            <tr>
                <td><input type="submit" value="Delete Employees"></td>
                <td><a href="${pageContext.request.contextPath}/employees">Back to Employees list</a></td>
            </tr>
        </tr>
        </table>
    </fieldset>
</form>
</body>
</html>