<%--
  Created by IntelliJ IDEA.
  User: BVMT
  Date: 12/11/2019
  Time: 1:19 CH
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create new customer</title>
    <style>
        .message{
            color:green;
        }
    </style>
</head>
<body>
<h1>Create new Products</h1>
<p>
    <c:if test='${requestScope["message"] != null}'>
        <span class="message">${requestScope["message"]}</span>
    </c:if>
</p>
<p>
    <a href="/employees">Back to Employees list</a>
</p>
<form method="post">
    <fieldset>
        <legend>Employees information</legend>
        <table>
            <td>Name: </td>
            <td><input type="text" name="name"></td>
            </tr>
            <tr>
                <td>Password: </td>
                <td><input type="text" name="password" ></td>
            </tr>

            <tr>
                <td>ID: </td>
                <td><input type="text" name="id_card_number"></td>
            </tr>
            <tr>
                <td>BirthDay: </td>
                <td><input type="text" name="birthday"></td>
            </tr>
            <tr>
                <td>gender: </td>
                <td><input type="text" name="gender"></td>
            </tr>
            <tr>
                <td>Authority: </td>
                <td><input type="text" name="authority"></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Create employees"></td>
            </tr>
        </table>
    </fieldset>
</form>
</body>
</html>