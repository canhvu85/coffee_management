<%--
  Created by IntelliJ IDEA.
  User: BVMT
  Date: 12/11/2019
  Time: 10:55 SA
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit Employees</title>
</head>
<body>
<h1>Edit Employees</h1>
<p>
    <c:if test='${requestScope["message"] != null}'>
        <span class="message">${requestScope["message"]}</span>
    </c:if>
</p>
<p>
    <a href="/employees">Back to employees list</a>
</p>
<form method="post">
    <fieldset>
        <legend>Employees information</legend>
        <table>
                    <tr>
                        <td>Name: </td>
                        <td><input type="text" name="name" id="name" value="${requestScope["employee"].getName()}"></td>
                    </tr>
                    <tr>
                        <td>Password: </td>
                        <td><input type="text" name="password" id="price" value="${requestScope["employee"].getPassword()}"></td>
                    </tr>

                    <tr>
                        <td>ID: </td>
                        <td><input type="text" name="id_card_number" id="description" value="${requestScope["employee"].getIdEmployee()}"></td>
                    </tr>
                    <tr>
                        <td>BirthDay: </td>
                        <td><input type="text" name="birthday" id="birthday" value="${requestScope["employee"].getBirthday()}"></td>
                    </tr>
                    <tr>
                        <td>gender: </td>
                <td>
                    <input type="text" name="gender" id="gender" value=<c:choose>
                        <c:when test="${employee.getGender()}">
                            Nam
                        </c:when>
                        <c:otherwise>
                            Nu
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <tr>
                <td>Authority: </td>
                <td><input type="text" name="authority" id="authority" value=<c:choose>
                    <c:when test="${employee.getAuthority()}">
                        Chu
                    </c:when>
                    <c:otherwise>
                        NhanVien
                    </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Update Employees"></td>
            </tr>
        </table>
    </fieldset>
</form>
</body>
</html>
