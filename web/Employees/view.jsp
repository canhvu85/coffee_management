<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View Employees</title>
</head>
<body>
<h1>Employees details</h1>
<p>
    <a href="/employees">Back to products list</a>
</p>
<table>
    <tr>
        <td>Name: </td>
        <td>${requestScope["employee"].getName()}</td>
    </tr>
    <tr>
        <td>Price: </td>
        <td>${requestScope["employee"].getPassword()}</td>
    </tr>
    <tr>
        <td>Id: </td>
        <td>${requestScope["employee"].getIdEmployee()}</td>
    </tr>
    <tr>
        <td>BirthDate:</td>
        <td>${requestScope["employee"].getBirthday()}</td>
    </tr>
    <tr>
        <td>Gender:</td>
        <td><c:choose>
            <c:when test="${requestScope['employee'].getGender()}">
                Nam
            </c:when>
            <c:otherwise>
                Nu
            </c:otherwise>
        </c:choose></td>
    </tr>
    <tr>
        <td>Authority:</td>
        <td>
            <c:choose>
            <c:when test='${requestScope["employee"].getAuthority()}'>
                Chu
            </c:when>
            <c:otherwise>
                Nhan Vien
            </c:otherwise>
        </c:choose>
        </td>
    </tr>
</table>
</body>
</html>
