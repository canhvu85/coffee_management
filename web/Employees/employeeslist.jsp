<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Vinh
  Date: 4/20/18
  Time: 6:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Employees List</title>
</head>
<body>
<h1>Employees</h1>
<p>
    <a href="${pageContext.request.contextPath}/employees?action=create">Create new Employees</a>
</p>
<table border="1">
    <tr>
        <td>Name</td>
        <td>BirthDate</td>
        <td>ID</td>
        <td>Authority</td>
        <td>Gender</td>
        <td>Edit</td>
        <td>Delete</td>

    </tr>
    <c:forEach items='${requestScope["employees"]}' var="employee">
        <tr>
            <td><a href="${pageContext.request.contextPath}/employees?action=view&id=${employee.getId()}">${employee.getName()}</a></td>
            <td>${employee.getBirthday()}</td>
            <td>${employee.getIdEmployee()}</td>
            <td><c:choose>
                <c:when test="${employee.getAuthority()}">
                    Chu
                </c:when>
                <c:otherwise>
                    Nhan Vien
                </c:otherwise>
            </c:choose></td>
            <td><c:choose>
                <c:when test="${employee.getGender()}">
                    Nam
                </c:when>
                <c:otherwise>
                    Nu
                </c:otherwise>
            </c:choose></td>
            <td><a href="${pageContext.request.contextPath}/employees?action=edit&id=${employee.getId()}">edit</a></td>
            <td><a href="${pageContext.request.contextPath}/employees?action=delete&id=${employee.getId()}">delete</a></td>

        </tr>
    </c:forEach>
</table>
<form method="post" action="${pageContext.request.contextPath}/employees?action=search">
    <input type="text" name="search">
    <button type="submit">Search</button>
</form>
</body>
</html>