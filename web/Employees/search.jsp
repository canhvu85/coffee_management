<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: BVMT
  Date: 06/11/2019
  Time: 3:21 CH
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Employees List Search</title>
</head>
<body>
<h1>Searched Employees</h1>
<p>
<p><a href="${pageContext.request.contextPath}/employees?action=create">Create new Employees</a></p>
<a href="/employees">Back to Employees list</a>
</p>
<table border="1">
    <tr><td>Name</td><td>Password</td><td>BirthDay</td><td>IdEmployee</td><td>Gender</td><td>Authority</td><td>Edit</td><td>Delete</td></tr>
    <c:forEach items='${requestScope["employees"]}' var="employee">

        <tr>
            <td>${employee.getName()}</td>
            <td>${employee.getPassword()}</td>
            <td>${employee.getBirthday()}</td>
            <td>${employee.getIdEmployee()}</td>
            <td><c:choose>
            <c:when test="${employee.getAuthority()}">
                Chu
            </c:when>
            <c:otherwise>
                Nhan Vien
            </c:otherwise>
            </c:choose></td>
            <td>
                <c:choose>
                <c:when test="${employee.getGender()}">
                    Nam
                </c:when>
                <c:otherwise>
                    Nu
                </c:otherwise>
                </c:choose>
            </td>
            <td><a href="${pageContext.request.contextPath}/employees?action=edit&id=${employee.getId()}">edit</a></td>
            <td><a href="${pageContext.request.contextPath}/employees?action=delete&id=${employee.getId()}">delete</a></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>

