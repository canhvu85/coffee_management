<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: PhanAnhVu
  Date: 11/12/2019
  Time: 10:05 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <%@include file="/headercss.jsp" %>
    <link rel="stylesheet" type="text/css" href="/Support/css/table-order.css">

    <title>View Bill Detail</title>
</head>
<body class="body">
<div class="main-menu">
    <jsp:include page="/main_menu.jsp" />
</div>

<div class="container show">
<h1>Bills Detail</h1>

<h2>Table: ${table}</h2>

<p>
    <a href="/bills">Back to bill list</a>
</p>

<table class="table">
    <thead>
        <tr>
            <th>Product</th>
            <th>Price</th>
            <th>Quantity</th>

        </tr>
    </thead>
    <tbody>
    <c:set var="total" value="${0}"/>
    <c:forEach items = '${requestScope["bills"]}' var="bill">
        <tr>

<%--            <td>${bill.get}</td>--%>
            <td>${bill.getName_product()}</td>
            <td>${bill.getPrice()}</td>
            <td>${bill.getQuantity()}</td>
            <c:set var="total" value="${total + bill.getPrice() * bill.getQuantity()}" />
        </tr>
    </c:forEach>
    </tbody>
    <tfoot>
        <tr>
            <td>Total</td>
            <td>${total}</td>
        </tr>
    </tfoot>

<%--    <form method="post" action="/bills?action=search">--%>
<%--        <input name="inputSearch" type="text">--%>
<%--        <input type="submit" value="search">--%>
<%--    </form>--%>

</table>
</div>
</body>
</html>
