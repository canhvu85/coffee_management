<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: PhanAnhVu
  Date: 11/11/2019
  Time: 4:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <%@include file="/headercss.jsp" %>
    <link rel="stylesheet" type="text/css" href="/Support/css/table-order.css">
    <title>Bills List</title>
</head>
<body class="body">
<div class="main-menu">
    <jsp:include page="/main_menu.jsp" />
</div>
<div class="container show">

<h1>Bills List</h1>

<table class="table" >
    <thead>
        <tr>
            <th>Bill id</th>
            <th>Time enter</th>
            <th>Time out</th>
            <th>Employee</th>
            <th>Table</th>
            <th>View</th>

        </tr>
    </thead>
    <tbody>
    <c:forEach items = '${requestScope["bills"]}' var="bill">
        <tr>
            <td><a href="/bills?action=view&id=${bill.getId_bill()}">${bill.getId_bill()}</a></td>
            <td>${bill.getTime_enter()}</td>
            <td>${bill.getTime_out()}</td>
            <td>${bill.getName_employee()}</td>
            <td>${bill.getName_table()}</td>
            <td><a href="/bills?action=view&id=${bill.getId_bill()}&table=${bill.getName_table()}">view</a></td>
        </tr>
    </c:forEach>
    </tbody>

<%--    <form method="post" action="/bills?action=search">--%>
<%--        <input name="inputSearch" type="text">--%>
<%--        <input type="submit" value="search">--%>
<%--    </form>--%>

</table>
</div>
</body>
</html>
