<%--
  Created by IntelliJ IDEA.
  User: ADMIN
  Date: 11/11/2019
  Time: 5:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Product List</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
<h1>Products</h1>
<p>
    <a href="/products?action=create">Create new product</a>
</p>
<table border="1" class="table table-striped">
    <tr>
        <td>Name</td>
        <td>Price</td>
    </tr>
    <c:forEach items='${requestScope["products"]}' var="product">
        <tr>
            <td><a href="products?action=view&id=${product.getId()}">${product.getName()}</a></td>
            <td>${product.getPrice()}</td>
            <td><a href="products?action=edit&id=${product.getId()}">Edit</a></td>
            <td><a href="products?action=delete&id=${product.getId()}">Delete</a></td>
        </tr>
    </c:forEach>
    <form action="/products">
        <input type="text" name="search">
        <input type="text" name="action" value="search" hidden>
        <input type="submit" value="Search">
    </form>
</table>
</div>
</body>
</html>