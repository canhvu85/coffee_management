package Utils;

import Model.Bill;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BillDB {
    public static List<Bill> findAll() throws SQLException, ClassNotFoundException {
        String sql = "select * " +
                "from bill " +
                "inner join employee on bill.id_employee = employee.id_employee " +
                "inner join table_coffee on bill.id_table = table_coffee.id_table " +
                "where bill.time_out  IS NOT NULL";
        Connection cnn = MySQLConnUtils.getSqlConnection();
        PreparedStatement ps = cnn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();

        List<Bill> list = new ArrayList<>();
        Bill bill = null;
        while (rs.next()){
            int id_bill = rs.getInt("id_bill");
            String time_enter = rs.getString("time_enter");
            String time_out = rs.getString("time_out");
            int id_employee = rs.getInt("id_employee");
            int id_table = rs.getInt("id_table");
            String name_employee = rs.getString("employee_name");
            String name_table = rs.getString("local");
            bill = new Bill(id_bill, time_enter, time_out, id_employee, id_table, name_employee, name_table);
            list.add(bill);
        }

        cnn.close();
        return list;
    }
}
