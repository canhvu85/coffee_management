package Service;

import Model.Table;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import Utils.MySQLConnUtils;
public class TableServiceImpl implements TableService {
    private void printSQLException(SQLException ex){
        for (Throwable e:ex){

            if(e instanceof SQLException){
                e.printStackTrace(System.err);
                System.err.println("SQLState:"+((SQLException) e).getSQLState());
                System.err.println("Error Code:"+((SQLException) e).getSQLState());
                System.err.println("Message: "+e.getMessage());
                Throwable t=ex.getCause();
                while (t!= null){
                    System.out.println("Cause: "+t);
                    t=t.getCause();
                }
            }
        }
    }

    @Override
    public List<Table> findAll() {
        List<Table> tables=new ArrayList<>();
        try (Connection connection=MySQLConnUtils.getSqlConnection();
             PreparedStatement preparedStatement=connection.prepareStatement("SELECT id_table,local,capacity,is_available FROM coffee.table_coffee WHERE is_delete=0;")
        ){
            System.out.println(preparedStatement);
            ResultSet rs=preparedStatement.executeQuery();
            while (rs.next()){
                int id=rs.getInt("id_table");
                String local=rs.getString("local");
                int capacity=rs.getInt("capacity");
                int is_available=rs.getInt("is_available");
                Table table=new Table(id,local,capacity,is_available);
                tables.add(table);
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();

        }
        return tables;
    }

    @Override
    public void save(Table table) {
        try (Connection connection=MySQLConnUtils.getSqlConnection();
        PreparedStatement preparedStatement=connection.prepareStatement("INSERT INTO coffee.table_coffee"+ "(local,capacity) VALUES"+" (?,?);")){
            preparedStatement.setString(1,table.getLocal());
            preparedStatement.setInt(2,table.getCapacity());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Table findById(int id_table) {
        Table table=null;
        try (Connection connection=MySQLConnUtils.getSqlConnection();
        PreparedStatement preparedStatement=connection.prepareStatement("SELECT id_table,local,capacity,is_available FROM coffee.table_coffee WHERE id_table=?;")){
            preparedStatement.setInt(1,id_table);
            ResultSet rs=preparedStatement.executeQuery();
            while (rs.next()){
                String local=rs.getString("local");
                int capacity=rs.getInt("capacity");
                int is_available=rs.getInt("is_available");
                table=new Table(id_table,local,capacity,is_available);
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return table;
    }

    @Override
    public void update(int id_table, Table table) {
        boolean rowUpdated;
        try (Connection connection=MySQLConnUtils.getSqlConnection();
        PreparedStatement preparedStatement=connection.prepareStatement("UPDATE coffee.table_coffee SET local=?,capacity=? WHERE id_table=?;")){
            preparedStatement.setString(1,table.getLocal());
            preparedStatement.setInt(2,table.getCapacity());
            preparedStatement.setInt(3,id_table);
            rowUpdated=preparedStatement.executeUpdate()>0;

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void remove(int id_table) {
        try(Connection connection=MySQLConnUtils.getSqlConnection();
        PreparedStatement preparedStatement=connection.prepareStatement("UPDATE coffee.table_coffee SET is_delete=1 WHERE id_table=?;")) {
            preparedStatement.setInt(1,id_table);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Table> findEmptyTable() {
        List<Table> tables=new ArrayList<>();
        try (Connection connection = MySQLConnUtils.getSqlConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT id_table,local,capacity,is_available FROM coffee.table_coffee WHERE is_available=1;")) {
            ResultSet rs=preparedStatement.executeQuery();
            while (rs.next()){
                int id_table=rs.getInt("id_table");
                String local=rs.getString("local");
                int capacity=rs.getInt("capacity");
                int is_available=rs.getInt("is_available");
                Table table=new Table(id_table,local,capacity,is_available);
                tables.add(table);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    return tables;
    }
}
